// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuGameModeBase.h"
#include "MenuGameStateBase.h"
#include "Kismet/GameplayStatics.h"
#include "SuperBeatFighterGameInstance.h"
#include "Android/AndroidDebugOutputDevice.h"

static int PLAYER_COUNT = 4;

void AMenuGameModeBase::Initialise(UMusicSpecifications * songs, AMenuGameStateBase * menuGameState)
{
	_songs = songs;
	_currentSongIndex = 0;

	menuGameState->_currentSong = _songs->musicConfigs[_currentSongIndex];

	const bool DOES_SPAWN_PAWN = true;

	for (int playerIndex = 0; playerIndex < PLAYER_COUNT; ++playerIndex)
	{
		APlayerController * createdPlayerController = UGameplayStatics::CreatePlayer(this, playerIndex, DOES_SPAWN_PAWN);

		//_playerControllers.Add(createdPlayerController);
	}
}

void AMenuGameModeBase::AddPlayerController(APlayerController* playerController)
{
	_playerControllers.Add(playerController);
}

void AMenuGameModeBase::PreviousSong(AMenuGameStateBase * menuGameState)
{
	if (_currentSongIndex > 0)
	{
		--_currentSongIndex;
		menuGameState->_currentSong = _songs->musicConfigs[_currentSongIndex];
	}
}

void AMenuGameModeBase::NextSong(AMenuGameStateBase * menuGameState)
{
	if (_currentSongIndex < _songs->musicConfigs.Num() - 2)
	{
		++_currentSongIndex;
		menuGameState->_currentSong = _songs->musicConfigs[_currentSongIndex];
	}
}

void AMenuGameModeBase::RegisterPlayerControllerInSlot(int playerSlotIndex, APlayerController* playerController)
{
	TArray<APlayerController*> playerControllerValues;

	_assignedPlayerControllers.GenerateValueArray(playerControllerValues);

	if (!playerControllerValues.Contains(playerController))
	{
		const int controllerId = UGameplayStatics::GetPlayerControllerID(playerController);

		_assignedPlayerControllers.Add(playerSlotIndex, playerController);

		UE_LOG(LogTemp, Display, TEXT("Player controller %d is assigned to player %d"), controllerId, playerSlotIndex);
	}
}

bool AMenuGameModeBase::IsPlayerControllerRegistered(APlayerController* playerController)
{
	TArray<APlayerController*> playerControllerValues;

	_assignedPlayerControllers.GenerateValueArray(playerControllerValues);

	return playerControllerValues.Contains(playerController);
}

void AMenuGameModeBase::ResetAllPlayers()
{
	_assignedPlayerControllers.Reset();
}

void AMenuGameModeBase::StartGameCpp(
	APlayerController* playerController, 
	USuperBeatFighterGameInstance* gameInstance, 
	AMenuGameStateBase* menuGameState
)
{
	const bool DOES_DESTROY_PAWN = true;
	TArray<APlayerController*> playerControllerValues;

	_assignedPlayerControllers.GenerateValueArray(playerControllerValues);

	if (playerControllerValues.Contains(playerController))
	{
		gameInstance->_selectedMusic = menuGameState->_currentSong;

		//for (APlayerController* currentPlayerController : _playerControllers)
		//{
		//	if (!playerControllerValues.Contains(currentPlayerController))
		//	{
		//		UGameplayStatics::RemovePlayer(currentPlayerController, DOES_DESTROY_PAWN);
		//	}
		//}

		for (const TTuple<int, APlayerController*> playerControllerTuple : _assignedPlayerControllers)
		{
			gameInstance->_players.Add(playerControllerTuple.Key, playerControllerTuple.Value->Player);
		}
	}
}
