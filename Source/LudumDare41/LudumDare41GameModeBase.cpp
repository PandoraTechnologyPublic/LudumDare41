// Fill out your copyright notice in the Description page of Project Settings.

#include "LudumDare41GameModeBase.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerStart.h"
#include "FightingCharacter.h"
#include "FightingPlayerController.h"
#include "FightingPlayerState.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "FightingGameState.h"
#include "SuperBeatFighterGameInstance.h"

void AFightingGameModeBase::InitialisePlayers(
	USuperBeatFighterGameInstance * gameInstance,
	TSubclassOf<AFightingCharacter> fightingCharacter,
	AFightingGameState * fightingGameState, 
	UFightingCharacterSpecifications * fightingCharacterSpecifications, 
	UBehaviorTree * behaviorTree, 
	AFightingCharacter *& leftPlayerFightingCharacter
)
{
	APlayerStart * leftPlayerStart = GetPlayerStart("PlayerStartLeft");
	APlayerStart * rightPlayerStart = GetPlayerStart("PlayerStartRight");
	AFightingPlayerState * leftPlayer;
	AFightingPlayerState * rightPlayer;
	
	leftPlayerFightingCharacter = CreateAndInitialisePlayerCpp(fightingCharacter, leftPlayerStart, 0, gameInstance->_players[0], fightingCharacterSpecifications);
	leftPlayer = Cast<AFightingPlayerState>(leftPlayerFightingCharacter->PlayerState);

	if (gameInstance->_players.Num() > 1)
	{
		rightPlayer = Cast<AFightingPlayerState>(CreateAndInitialisePlayerCpp(fightingCharacter, rightPlayerStart, 1, gameInstance->_players[1], fightingCharacterSpecifications)->PlayerState);
	}
	else
	{
		rightPlayer = Cast<AFightingPlayerState>(CreateAndInitialiseAiCpp(fightingCharacter, rightPlayerStart, 1, behaviorTree, fightingCharacterSpecifications)->PlayerState);
	}

	fightingGameState->playerOneState = leftPlayer;
	fightingGameState->playerTwoState = rightPlayer;
}

APlayerStart* AFightingGameModeBase::GetPlayerStart(FName playerStartTag)
{
	APlayerStart * foundPlayerStart = nullptr;
	TArray<AActor*> playerStarts;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), playerStarts);

	for (AActor * playerStartActor : playerStarts)
	{
		APlayerStart * playerStart = Cast<APlayerStart>(playerStartActor);
		
		if (playerStart != nullptr)
		{
			if (playerStart->PlayerStartTag.Compare(playerStartTag) == 0)
			{
				foundPlayerStart = playerStart;
				break;
			}
		}
	}

	return foundPlayerStart;
}

AFightingCharacter * AFightingGameModeBase::CreateAndInitialisePlayerCpp(
	TSubclassOf<AFightingCharacter> fightingCharacter,
	APlayerStart * playerStart, 
	int selectedCharacterIndex, 
	UPlayer * player, 
	UFightingCharacterSpecifications * fightingCharacterSpecifications
)
{
	AFightingCharacter * spawnedPlayer = GetWorld()->SpawnActor<AFightingCharacter>(fightingCharacter, playerStart->GetActorTransform());

	player->PlayerController->Possess(spawnedPlayer);

	InitialisePlayerCpp(player->PlayerController, selectedCharacterIndex, FString::Printf(TEXT("Player %d"), selectedCharacterIndex), playerStart, fightingCharacterSpecifications);

	return spawnedPlayer;
}

AFightingCharacter * AFightingGameModeBase::CreateAndInitialiseAiCpp(
	TSubclassOf<AFightingCharacter> fightingCharacter,
	APlayerStart * playerStart, 
	int selectedCharacterIndex, 
	UBehaviorTree * behaviorTree, 
	UFightingCharacterSpecifications * fightingCharacterSpecifications
)
{
	AFightingCharacter * spawnedPlayer = Cast<AFightingCharacter>(UAIBlueprintHelperLibrary::SpawnAIFromClass(GetWorld(), fightingCharacter, behaviorTree, playerStart->GetActorLocation(), playerStart->GetActorRotation()));
	
	InitialisePlayerCpp(spawnedPlayer->Controller, selectedCharacterIndex, "Ai Player", playerStart, fightingCharacterSpecifications);

	return spawnedPlayer;
}

void AFightingGameModeBase::InitialisePlayerCpp(
	AController * controller, 
	int characterId, 
	FString playerName, 
	APlayerStart * playerStart, 
	UFightingCharacterSpecifications * fightingCharacterSpecifications
)
{
	AFightingPlayerState * fightingPlayerState = Cast<AFightingPlayerState>(controller->PlayerState);
	AFightingCharacter * fightingCharacter = Cast<AFightingCharacter>(controller->GetPawn());

	fightingPlayerState->_playerStart = playerStart;
	fightingPlayerState->InitialiseCpp(playerName, fightingCharacterSpecifications->characterConfigs[characterId]);
}
