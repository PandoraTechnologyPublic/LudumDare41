// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MusicSpecifications.h"
#include "SuperBeatFighterGameInstance.generated.h"

class UPlayer;

/**
 * 
 */
UCLASS()
class LUDUMDARE41_API USuperBeatFighterGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite) TMap<int, UPlayer*> _players;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) FMusicConfig _selectedMusic;
};
