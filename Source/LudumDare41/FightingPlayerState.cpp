// Fill out your copyright notice in the Description page of Project Settings.

#include "FightingPlayerState.h"
#include "GameFramework/PlayerStart.h"


void AFightingPlayerState::InitialiseCpp(FString name, FFightingCharacterConfig characterConfig)
{
	_name = name;
	_characterConfig = characterConfig;
}

float AFightingPlayerState::GetHealth() const
{
	return _health / static_cast<float>(_characterConfig.maxHealth);
}

FString AFightingPlayerState::GetName() const
{
	return _name;
}

int AFightingPlayerState::GetHaircutIndex() const
{
	return _characterConfig.haircutIndex;
}

bool AFightingPlayerState::GetIsGirl() const
{
	return _characterConfig.isGirl;
}

UTexture2D * AFightingPlayerState::GetAvatar() const
{
	return _characterConfig.avatar;
}

bool AFightingPlayerState::IsRightPlayer() const
{
	bool isRightPlayer = false;

	if (_playerStart != nullptr)
	{
		isRightPlayer = _playerStart->PlayerStartTag.IsEqual(FName("PlayerStartRight"));
	}
	else
	{
		UE_LOG(LogTemp, Display, TEXT("_playerStart is null"));
	}

	return isRightPlayer;
}

void AFightingPlayerState::ImpactHealth(float power, float precision)
{
	health -= power * precision;
}
