// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "FightingGameState.generated.h"

class AFightingPlayerState;
/**
 * 
 */
UCLASS()
class LUDUMDARE41_API AFightingGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite) AFightingPlayerState * playerOneState;
	UPROPERTY(BlueprintReadWrite) AFightingPlayerState * playerTwoState;
};
