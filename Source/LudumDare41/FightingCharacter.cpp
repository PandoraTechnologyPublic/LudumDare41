// Fill out your copyright notice in the Description page of Project Settings.

#include "FightingCharacter.h"
#include "FightingAIController.h"
#include "Components/InputComponent.h"


// Sets default values
AFightingCharacter::AFightingCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AIControllerClass = AFightingAIController::StaticClass();
}

// Called when the game starts or when spawned
void AFightingCharacter::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void AFightingCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AFightingCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
//
//	InputComponent->BindAxis("Move", this, &AFightingCharacter::Move);
//
}
//
//void AFightingCharacter::Move(float AxisValue)
//{
//	AddMovementInput(FVector(0, 1, 0), AxisValue);
//}

