// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "UtilsBlueprintFunctions.generated.h"

/**
 * 
 */
UCLASS()
class LUDUMDARE41_API UUtilsBlueprintFunctions : public UObject
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable) void PrintArray(TArray<UObject *> array);	
};
