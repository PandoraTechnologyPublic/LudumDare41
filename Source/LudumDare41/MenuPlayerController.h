// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MenuPlayerController.generated.h"

class AMenuGameModeBase;
/**
 * 
 */
UCLASS()
class LUDUMDARE41_API AMenuPlayerController : public APlayerController
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable) void Initialise(AMenuGameModeBase * gameMode);
	UFUNCTION(BlueprintCallable) void OnStartButton(AMenuGameModeBase * gameMode);
	UFUNCTION(BlueprintCallable) void OnBackButton(AMenuGameModeBase * gameMode);
	UFUNCTION(BlueprintCallable) void OnContinueButton(AMenuGameModeBase * gameMode);
	UFUNCTION(BlueprintCallable) void IsRegistered(AMenuGameModeBase * gameMode);
	
	
	
};
