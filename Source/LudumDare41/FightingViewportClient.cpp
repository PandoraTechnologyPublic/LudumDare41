// Fill out your copyright notice in the Description page of Project Settings.

#include "FightingViewportClient.h"


bool UFightingViewportClient::InputKey(FViewport* Viewport, int32 ControllerId, FKey Key, EInputEvent EventType, float AmountDepressed, bool bGamepad)
{
	//UE_LOG(LogTemp, Display, TEXT("ControllerId is %s"), *Key.ToString());
	return UGameViewportClient::InputKey(Viewport, ControllerId, Key, EventType, AmountDepressed, bGamepad);
}

bool UFightingViewportClient::InputAxis(FViewport* Viewport, int32 ControllerId, FKey Key, float Delta, float DeltaTime, int32 NumSamples, bool bGamepad)
{
	//UE_LOG(LogTemp, Display, TEXT("ControllerId is %s"), *Key.ToString());
	return  UGameViewportClient::InputAxis(Viewport, ControllerId, Key, Delta, DeltaTime, NumSamples, bGamepad);
}
