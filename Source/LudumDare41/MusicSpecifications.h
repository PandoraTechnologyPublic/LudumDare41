// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Components/AudioComponent.h"

#include "MusicSpecifications.generated.h"


class USoundWave;


USTRUCT(BlueprintType)
struct FMusicConfig
{
	GENERATED_USTRUCT_BODY()

	public:
		UPROPERTY(EditAnywhere, BlueprintReadOnly) FString name;
		UPROPERTY(EditAnywhere, BlueprintReadOnly) float tempo;
		UPROPERTY(EditAnywhere, BlueprintReadOnly) float playbackStartTime;
		UPROPERTY(EditAnywhere, BlueprintReadOnly) int beatsBeforeStart;
		UPROPERTY(EditAnywhere, BlueprintReadOnly) int sequenceIndex;
	
};

/**
 * 
 */
UCLASS(BlueprintType)
class LUDUMDARE41_API UMusicSpecifications : public UDataAsset
{
	GENERATED_BODY()
	
	public:
		UPROPERTY(EditAnywhere, BlueprintReadOnly) TArray<FMusicConfig> musicConfigs;
	
};
