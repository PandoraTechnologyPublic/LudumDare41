// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "FightingCharacterSpecifications.h"
#include "FightingPlayerState.generated.h"

class APlayerStart;

/**
 * 
 */
UCLASS()
class LUDUMDARE41_API AFightingPlayerState : public APlayerState
{
	GENERATED_BODY()
	
	public:
		UPROPERTY(BlueprintReadWrite) int health;
		UPROPERTY(BlueprintReadWrite) int initialHealth;
		UPROPERTY(BlueprintReadWrite) int wonRounds;
		UPROPERTY(BlueprintReadWrite) APlayerStart * _playerStart;

		UFUNCTION(BlueprintCallable) void InitialiseCpp(FString name, FFightingCharacterConfig characterConfig);
		UFUNCTION(BlueprintCallable) float GetHealth() const;
		UFUNCTION(BlueprintCallable) FString GetName() const;
		UFUNCTION(BlueprintCallable) int GetHaircutIndex() const;
		UFUNCTION(BlueprintCallable) bool GetIsGirl() const;
		UFUNCTION(BlueprintCallable) UTexture2D * GetAvatar() const;
		UFUNCTION(BlueprintCallable) bool IsRightPlayer() const;
		UFUNCTION(BlueprintCallable) void ImpactHealth(float power, float precision);

	private:
		FString _name;
		FFightingCharacterConfig _characterConfig;
		int _health;

};
