// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LudumDare41GameModeBase.generated.h"

class UFightingCharacterSpecifications;
class AFightingGameState;
class AFightingCharacter;
class AFightingPlayerController;
class AFightingPlayerState;
class APlayerStart;
class UBehaviorTree;
class USuperBeatFighterGameInstance;
/**
 * 
 */
UCLASS()
class LUDUMDARE41_API AFightingGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable) void InitialisePlayers(USuperBeatFighterGameInstance * gameInstance, TSubclassOf<AFightingCharacter> fightingCharacter, AFightingGameState * fightingGameState, UFightingCharacterSpecifications * fightingCharacterSpecifications, UBehaviorTree * behaviorTree, AFightingCharacter *& leftPlayerFightingCharacter);
	UFUNCTION(BlueprintCallable) void InitialisePlayerCpp(AController * controller, int characterId, FString playerName, APlayerStart* playerStart, UFightingCharacterSpecifications * fightingCharacterSpecifications);
	UFUNCTION(BlueprintCallable) AFightingCharacter * CreateAndInitialisePlayerCpp(TSubclassOf<AFightingCharacter> fightingCharacter, APlayerStart * playerStart, int selectedCharacterIndex, UPlayer * player, UFightingCharacterSpecifications * fightingCharacterSpecifications);
	UFUNCTION(BlueprintCallable) AFightingCharacter * CreateAndInitialiseAiCpp(TSubclassOf<AFightingCharacter> fightingCharacter, APlayerStart * playerStart, int selectedCharacterIndex, UBehaviorTree * behaviorTree, UFightingCharacterSpecifications * fightingCharacterSpecifications);
	UFUNCTION(BlueprintCallable) APlayerStart * GetPlayerStart(FName playerStartTag);

	TArray<AFightingPlayerController*> _playerControllers;

};
