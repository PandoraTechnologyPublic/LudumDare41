// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "FightingCharacterSpecifications.generated.h"
class UTexture2D;

USTRUCT(BlueprintType)
struct FFightingCharacterConfig 
{
	GENERATED_USTRUCT_BODY()

	public:
		UPROPERTY(EditAnywhere, BlueprintReadOnly) FString name;
		UPROPERTY(EditAnywhere, BlueprintReadOnly) int maxHealth;
		UPROPERTY(EditAnywhere, BlueprintReadOnly) UTexture2D* avatar;
		UPROPERTY(EditAnywhere, BlueprintReadOnly) bool isGirl;
		UPROPERTY(EditAnywhere, BlueprintReadOnly) int haircutIndex;
};

/**
 * 
 */
UCLASS(BlueprintType)
class LUDUMDARE41_API UFightingCharacterSpecifications : public UDataAsset
{
	GENERATED_BODY()

	public:
		UPROPERTY(EditAnywhere, BlueprintReadOnly) TArray<FFightingCharacterConfig> characterConfigs;
};
