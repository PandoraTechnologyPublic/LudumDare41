// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameViewportClient.h"
#include "FightingViewportClient.generated.h"

/**
 * 
 */
UCLASS()
class LUDUMDARE41_API UFightingViewportClient : public UGameViewportClient
{
	GENERATED_BODY()


public:
	bool InputKey(FViewport* Viewport, int32 ControllerId, FKey Key, EInputEvent EventType, float AmountDepressed, bool bGamepad) override;
	bool InputAxis(FViewport* Viewport, int32 ControllerId, FKey Key, float Delta, float DeltaTime, int32 NumSamples, bool bGamepad) override;
};
