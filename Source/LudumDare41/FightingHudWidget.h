// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FightingHudWidget.generated.h"

class UTextBlock;
class UCanvasPanel;

/**
 * 
 */
UCLASS()
class LUDUMDARE41_API UFightingHudWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)  TSubclassOf<UWidget> beatElementPrefab;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) int beatDisplayedCount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float startScale;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float endScale;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float endPositionOffset;

	UFUNCTION(BlueprintCallable) void SetHitSuccessText(UTextBlock * textElement, float precision) const;
	UFUNCTION(BlueprintCallable) void UpdateBeatTime(float tempo, float timeSinceLastBeat);
	UFUNCTION(BlueprintCallable) void HandleVisualBeat(UCanvasPanel * beatCanvas, bool isRightPlayer);
private:

	UWidget * GenerateNewBeatElement(TArray<UWidget*> * beatElements) const;
	void GenerateNewBeatElements(UCanvasPanel* beatCanvas, TArray<UWidget*>* beatElements, int beatCount, bool isRightPlayer) const;
	void UpdateBeatElements(TArray<UWidget*>* array, int beatDisplayedCount, bool isRightPlayer, float currentProgress) const;

	float timeSinceStart;
	float tempo;
	TArray<UWidget *> leftBeatElements; 
	TArray<UWidget *> rightBeatElements; 
};
