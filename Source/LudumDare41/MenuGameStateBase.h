// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "MusicSpecifications.h"
#include "MenuGameStateBase.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
 enum class MenuModeType : uint8
{
	MENU_MODE_MAIN_MENU,
	MENU_MODE_CONFIG_MENU
};

UCLASS()
class LUDUMDARE41_API AMenuGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite) MenuModeType _menuMode = MenuModeType::MENU_MODE_MAIN_MENU;
	UPROPERTY(BlueprintReadWrite) FMusicConfig _currentSong;

	UFUNCTION(BlueprintCallable) void Initialise(FMusicConfig currentSong);
	UFUNCTION(BlueprintCallable) void GoToMenuMode(MenuModeType menuMode);
};
