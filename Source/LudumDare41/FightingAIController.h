// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "FightingAIController.generated.h"

/**
 * 
 */
UCLASS()
class LUDUMDARE41_API AFightingAIController : public AAIController
{
	GENERATED_BODY()


public:
	AFightingAIController();
	UFUNCTION(BlueprintCallable) void SetWantsPlayerState(bool doesWantsPlayerState);
};
