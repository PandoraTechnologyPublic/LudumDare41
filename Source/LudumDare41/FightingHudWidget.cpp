// Fill out your copyright notice in the Description page of Project Settings.

#include "FightingHudWidget.h"
#include "Components/CanvasPanel.h"
#include "UserWidget.h"
#include "TextBlock.h"
#include "CanvasPanelSlot.h"
#include "CoreMinimal.h"

#define LOCTEXT_NAMESPACE "LudumDare41" 

void UFightingHudWidget::SetHitSuccessText(UTextBlock * textElement, float precision) const
{
	if (precision < 0.5f)
	{
		textElement->SetText(LOCTEXT("Missed","Missed"));
	}
	else if (precision < 0.6f)
	{
		textElement->SetText(LOCTEXT("ok", "ok"));
	}
	else if (precision < 0.8f)
	{
		textElement->SetText(LOCTEXT("Good", "Good"));
	}
	else
	{
		textElement->SetText(LOCTEXT("Perfect", "Perfect"));
	}
}

#undef LOCTEXT_NAMESPACE

void UFightingHudWidget::UpdateBeatTime(float tempo, float timeSinceLastBeat)
{
	timeSinceStart = timeSinceLastBeat;
	this->tempo = tempo;
}

void UFightingHudWidget::HandleVisualBeat(UCanvasPanel * beatCanvas, bool isRightPlayer)
{
	const float musicBeatDuration = 60.0f / tempo;
	const float currentProgress = timeSinceStart / musicBeatDuration;

	if (isRightPlayer)
	{
		GenerateNewBeatElements(beatCanvas, &rightBeatElements, beatDisplayedCount, isRightPlayer);
		UpdateBeatElements(&rightBeatElements, beatDisplayedCount, isRightPlayer, currentProgress);
	}
	else
	{
		GenerateNewBeatElements(beatCanvas, &leftBeatElements, beatDisplayedCount, isRightPlayer);
		UpdateBeatElements(&leftBeatElements, beatDisplayedCount, isRightPlayer, currentProgress);
	}

	////UE_LOG(LogTemp, Display, TEXT("HandleVisualBeat"));

	////UE_LOG(LogTemp, Display, TEXT("currentProgress is %f"), currentProgress);
}

UWidget * UFightingHudWidget::GenerateNewBeatElement(TArray<UWidget *> * beatElements) const
{
	UWidget * spawnedWidget = CreateWidget<UUserWidget>(GetWorld(), beatElementPrefab);

	beatElements->Add(spawnedWidget);

	return spawnedWidget;
}

void UFightingHudWidget::GenerateNewBeatElements(UCanvasPanel * beatCanvas, TArray<UWidget*> * beatElements, int beatCount, bool isRightPlayer) const
{
	while (beatElements->Num() < beatCount)
	{
		UWidget * newBeatElement = GenerateNewBeatElement(beatElements);
		UCanvasPanelSlot * slot = beatCanvas->AddChildToCanvas(newBeatElement);

		if (isRightPlayer)
		{
			slot->SetAlignment(FVector2D(0.0f, 0.5f));
			slot->SetAnchors(FAnchors(0, 0.5f, 0, 0.5f));
		}
		else
		{
			slot->SetAlignment(FVector2D(1.0f, 0.5f));
			slot->SetAnchors(FAnchors(1.0f, 0.5f, 1.0f, 0.5f));
		}

		slot->SetSize(FVector2D(128, 128));
		slot->SetPosition(FVector2D(0, 0));
	}
}

void UFightingHudWidget::UpdateBeatElements(TArray<UWidget*>* beatElements, int beatDisplayedCount, bool isRightPlayer, float currentProgress) const
{
	const FVector2D startScaleVector(startScale, startScale);
	const FVector2D endScaleVector(endScale - startScale, endScale - startScale);
	const FVector2D startPosition(endPositionOffset, 0);
	const FVector2D beatElementOffestPosition = startPosition / beatDisplayedCount;
	const FVector2D beatElementOffsetScale = endScaleVector / beatDisplayedCount;
	int beatElementIndex = 0;

	for (UWidget * beatElement : *beatElements)
	{
		FVector2D currentPosition = startPosition - ((beatElementOffestPosition * currentProgress) + beatElementOffestPosition * beatElementIndex);
		FVector2D currentScale = startScaleVector + beatElementOffsetScale * currentProgress + beatElementOffsetScale * beatElementIndex;

		if (isRightPlayer)
		{
			currentPosition *= -1;
		}

		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, currentPosition.ToString());

		beatElement->SetRenderTranslation(currentPosition);
		beatElement->SetRenderScale(currentScale);
		
		++beatElementIndex;
	}
}
