// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuGameStateBase.h"
#include <cassert>


void AMenuGameStateBase::Initialise(FMusicConfig  currentSong)
{
	_currentSong = currentSong;
}

void AMenuGameStateBase::GoToMenuMode(MenuModeType menuMode)
{
	assert(_menuMode != menuMode);
	_menuMode = menuMode;
}
