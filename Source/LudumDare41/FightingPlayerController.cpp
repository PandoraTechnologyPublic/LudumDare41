// Fill out your copyright notice in the Description page of Project Settings.

#include "FightingPlayerController.h"
#include "LudumDare41GameModeBase.h"

void AFightingPlayerController::Register(AFightingGameModeBase* fightingGameMode)
{
	fightingGameMode->_playerControllers.Add(this);
}
