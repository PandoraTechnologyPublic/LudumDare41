// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MusicSpecifications.h"
#include "MenuGameModeBase.generated.h"

class USuperBeatFighterGameInstance;
class AMenuGameStateBase;
/**
 * 
 */
UCLASS()
class LUDUMDARE41_API AMenuGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	

	UFUNCTION(BlueprintCallable) void Initialise(UMusicSpecifications * songs, AMenuGameStateBase * menuGameState);
	UFUNCTION(BlueprintCallable) void AddPlayerController(APlayerController* playerController);
	UFUNCTION(BlueprintCallable) void PreviousSong(AMenuGameStateBase * menuGameState);
	UFUNCTION(BlueprintCallable) void NextSong(AMenuGameStateBase * menuGameState);
	UFUNCTION(BlueprintCallable) void RegisterPlayerControllerInSlot(int playerSlotIndex, APlayerController* playerController);
	UFUNCTION(BlueprintCallable) bool IsPlayerControllerRegistered(APlayerController* playerController);
	UFUNCTION(BlueprintCallable) void ResetAllPlayers();
	UFUNCTION(BlueprintCallable) void StartGameCpp(APlayerController* playerController, USuperBeatFighterGameInstance* gameInstance, AMenuGameStateBase * menuGameState);
private:
	TMap<int, APlayerController*> _assignedPlayerControllers;
	TArray<APlayerController*> _playerControllers;
	UMusicSpecifications * _songs = nullptr;
	int _currentSongIndex = 0;
};
