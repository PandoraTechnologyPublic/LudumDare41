// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FightingPlayerController.generated.h"

class APlayerStart;
class AFightingGameModeBase;

/**
 * 
 */
UCLASS()
class LUDUMDARE41_API AFightingPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable) void Register(AFightingGameModeBase * fightingGameMode);
};
