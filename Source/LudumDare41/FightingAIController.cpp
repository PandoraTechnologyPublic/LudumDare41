// Fill out your copyright notice in the Description page of Project Settings.

#include "FightingAIController.h"

AFightingAIController::AFightingAIController()
{
	SetWantsPlayerState(true);
}

void AFightingAIController::SetWantsPlayerState(bool doesWantsPlayerState)
{
	bWantsPlayerState = doesWantsPlayerState;
}
