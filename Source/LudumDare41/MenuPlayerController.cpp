// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuPlayerController.h"
#include "MenuGameModeBase.h"


void AMenuPlayerController::Initialise(AMenuGameModeBase* gameMode)
{
	gameMode->AddPlayerController(this);
}

void AMenuPlayerController::OnStartButton(AMenuGameModeBase * gameMode)
{

}

void AMenuPlayerController::OnBackButton(AMenuGameModeBase * gameMode)
{
}

void AMenuPlayerController::OnContinueButton(AMenuGameModeBase * gameMode)
{
}

void AMenuPlayerController::IsRegistered(AMenuGameModeBase* gameMode)
{
	gameMode->IsPlayerControllerRegistered(this);
}
